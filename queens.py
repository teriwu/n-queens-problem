from pprint import pprint
from tkinter import N

"""
approache

- start with empty board
- for each rook, place on first safe spot
- start with empty board
- for 1 to N (4)
    - for pos in 1 to N
        if pos is safe ???
            place piece
            break out of inner loop
"""
n = 8

def pos_is_safe(board, col): # row = 1, col = 1
    # is there something in the column
    for row in range(0,4):
        if board[row][col] != None:
            return False

    # check for diagonal
    for i in range(0,4):
        if board[i][i] != None:
            return False

    # check for diagonal #2
    for i in range(0,4):
        if board[3-i][i] != None:
            return False

    return True

def place_piece(board, row, col):
    board[row][col] = "1"

def is_solution_found(board):
    pass

def queens(n):
    row = [0 for x in range(0, n)]
    board = [row for y in range(0, n)]

    while True:
        if is_solution_found(board):
            break
        else:
            for row in range(0, n):
                for col in range(0, n):
                    if pos_is_safe(board, col):
                        place_piece(board, row, col)
                        break

    return board


if __name__ == "__main__":
    board = queens()
    pprint(board, width=40)